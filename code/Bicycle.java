// Paul Patrick Rabanal - 1932414
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return manufacturer;
    }

    public int getNumberGears(){
        return numberGears;
    }

    public double getMaxSpeed(){
        return maxSpeed;
    }

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
} 

    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}