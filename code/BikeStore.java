// Paul Patrick Rabanal - 1932414
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] store = new Bicycle[4];

        store[0] = new Bicycle("Trek", 24, 40);
        store[1] = new Bicycle("Giant", 21, 50);
        store[2] = new Bicycle("Norco", 27, 55);
        store[3] = new Bicycle("Schwinn", 18, 45);

        for (int i = 0; i < store.length; i++) {
            System.out.println(store[i]);
        }
    }
}
